﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWpf
{
    public delegate double BinaryMathOperation(double x, double y);
    public delegate double UnaryMathOperation(double x);

    public class CalcModel
    {
        public double operand1 { get; set; }
        public double operand2 { get; set; }
        public double result { get; set; }

        public CalcModel()
        {
            operand1 = 0;
            operand2 = 0;
            result = 0;
        }

        public void doCalc(BinaryMathOperation op)
        {
            result = op(operand1, operand2);
        }

        public void doCalc(UnaryMathOperation op)
        {
            result = op(operand1);
        }
    }
}
