﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections.ObjectModel;
using System.Windows.Input;
using GalaSoft.MvvmLight;

namespace SimpleWpf
{
    public class Book : ObservableObject
    {
        private string _title;
        private int _pages;

        public string Title {
            get
            {
                return _title;
            }

            set
            {
                _title = value;
                RaisePropertyChanged(() => _title);
            } 
        
        }

        public int Pages 
        {
            get 
            {
                return _pages;
            }

            set
            {
                _pages = value;
                RaisePropertyChanged(() => _pages);
            }
        
        }
    }
}
