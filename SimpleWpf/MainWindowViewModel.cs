﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight;


namespace SimpleWpf
{
    public class MainWindowViewModel : ViewModelBase
    {
        /*
        public double X { get; set; }
        public double Y { get; set; }
        private double _z;

        public double Z
        {
            get { return _z; }
            set 
            { 
                _z = value;
                RaisePropertyChanged(() => Z); 
            }
        }
        public MainWindowViewModel()
        {
            X = 1;
            Y = 2;
            Z = 3;
        }

        ICommand _calc;
        public ICommand Calc
        {
            get { return _calc ?? (_calc = new RelayCommand(() => Z = X + Y)); }
        }
        */

        public ObservableCollection<Book> Books { get; set; }

        ICommand _addCommand;
        public ICommand AddCommand {
            get
            {
                return _addCommand ?? (new RelayCommand(() => { 
                    Books.Add(new Book { Title = "New Book", Pages = 123 });
                    RaisePropertyChanged();
                }));
            }
        }

        ICommand _editCommand;
        public ICommand EditCommand
        {
            get
            {
                return _editCommand ?? (new RelayCommand(() =>
                {
                    Books.First().Title += " changed";
                    //Books.First().Pages += 100;
                    RaisePropertyChanged();
                }));
            }
        }


        ICommand _deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return _deleteCommand ?? (new RelayCommand(() =>
                {
                    Books.RemoveAt(Books.Count - 1);
                    RaisePropertyChanged();
                }));
            }
        }





        public MainWindowViewModel()
        {
            Books = new ObservableCollection<Book>()
            {
                new Book { Title = "WPF learning", Pages = 100 }
                , new Book { Title = "WCF learning", Pages = 150 }
                , new Book { Title = "EF learning", Pages = 150 }
            };
        }
    }
}
